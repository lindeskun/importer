<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Model;

class DataFile extends Model
{
    use HasTimestamps;

    public const STATUS_READY_TO_IMPORT = 0;
    public const STATUS_IMPORT_STARTED = 1;
    public const STATUS_IMPORT_FINISHED = 2;
    public const STATUS_IMPORT_ERROR = 3;

    protected $guarded = [];

    public function isFinished(): bool
    {
        return $this->getAttribute('status') === self::STATUS_IMPORT_FINISHED;
    }

    public function startImport(): void
    {
        $this->setAttribute('status', self::STATUS_IMPORT_STARTED);
        $this->save();
    }

    public function finishImport(): void
    {
        $this->setAttribute('status', self::STATUS_IMPORT_FINISHED);
        $this->save();
    }

    public function finishImportWithError(): void
    {
        $this->setAttribute('status', self::STATUS_IMPORT_ERROR);
        $this->save();
    }
}
