<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasTimestamps;

    protected $primaryKey = 'uuid';
    protected $keyType = 'string';
    protected $guarded = [];
}
