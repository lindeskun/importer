<?php

namespace App\Models;

use betterapp\LaravelDbEncrypter\Traits\EncryptableDbAttribute;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
    use EncryptableDbAttribute;
    use HasTimestamps;

    protected $primaryKey = 'uuid';
    protected $keyType = 'string';

    protected $encryptable = [
        'number',
        'expirationDate',
    ];

    public static function makeDateTimeFromExpiration(string $expiresAt): Carbon
    {
        [$month, $year] = explode('/', $expiresAt);
        $dateTime = new Carbon();

        $dateTime
            ->setYear($year)
            ->setMonth($month)
            ->endOfMonth();

        return $dateTime;
    }
}
