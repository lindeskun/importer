<?php

declare(strict_types=1);

namespace App\Models\FileReaders;

use Illuminate\Support\LazyCollection;

class JsonFileReader implements FileReaderInterface
{
    public function getLazyCollection(string $filePath): LazyCollection
    {
        return lazyJson($filePath);
    }

    public function supports(string $mimeType): bool
    {
        return 'application/json' === $mimeType;
    }
}