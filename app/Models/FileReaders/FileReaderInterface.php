<?php

declare(strict_types=1);

namespace App\Models\FileReaders;

use Illuminate\Support\LazyCollection;

interface FileReaderInterface
{
    public function supports(string $mimeType): bool;
    public function getLazyCollection(string $filePath): LazyCollection;
}