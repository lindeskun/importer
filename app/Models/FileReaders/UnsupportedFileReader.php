<?php

declare(strict_types=1);

namespace App\Models\FileReaders;

use Illuminate\Support\LazyCollection;

class UnsupportedFileReader implements FileReaderInterface
{
    public function supports(string $mimeType): bool
    {
        return false;
    }

    public function getLazyCollection(string $filePath): LazyCollection
    {
        return new LazyCollection();
    }
}