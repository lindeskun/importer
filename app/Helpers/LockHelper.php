<?php

declare(strict_types=1);


namespace App\Helpers;

use Illuminate\Database\Eloquent\Model;

class LockHelper
{
    public static function makeLockKey(Model $model): string
    {
        return sprintf('%s_%s', $model->getTable(), $model->getKey());
    }
}