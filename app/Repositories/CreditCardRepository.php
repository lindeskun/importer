<?php

declare(strict_types=1);


namespace App\Repositories;

use App\Models\CreditCard;

class CreditCardRepository
{
    public function batchCreate(array $items): bool
    {
        return CreditCard::query()->insert($items);
    }
}