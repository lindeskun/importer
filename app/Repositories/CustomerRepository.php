<?php

declare(strict_types=1);


namespace App\Repositories;

use App\Models\Customer;

class CustomerRepository
{
    public function batchCreate(array $items): bool
    {
        return Customer::query()->insert($items);
    }
}