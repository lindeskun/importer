<?php

declare(strict_types=1);


namespace App\Repositories;

use App\Models\DataFile;

class DataFileRepository
{
    public function findByHash(string $hash): ?DataFile
    {
        /** @var DataFile|null $model */
        $model = DataFile::query()->where('hash', '=', $hash)->first();

        return $model;
    }

    public function unfinishedImportsByChunk(int $count, callable $callable)
    {
        DataFile::query()
            ->where('status', '=', DataFile::STATUS_IMPORT_STARTED)
            ->chunk($count, $callable);
    }

    public function create(string $hash, string $type): DataFile
    {
        /** @var DataFile $model */
        $model = DataFile::query()->create([
            'hash' => $hash,
            'type' => $type,
            'status' => DataFile::STATUS_READY_TO_IMPORT,
        ]);

        return $model;
    }
}