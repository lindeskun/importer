<?php

declare(strict_types=1);

namespace App\Services;

use App\Helpers\LockHelper;
use App\Models\CreditCard;
use App\Models\DataFile;
use App\PlainObjects\ImportCollector;
use App\Repositories\CreditCardRepository;
use App\Repositories\CustomerRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Cache\Lock;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Ramsey\Uuid\Uuid;

class ImportService
{
    private const DEFAULT_LOCK_TTL = 300;
    private const CHUNK_SIZE = 50;

    private const MINIMAL_AGE = 18;
    private const MAXIMAL_AGE = 65;

    public function __construct(
        private FilesService $dataFilesService,
        private FileReaderService $fileReaderService,
        private CustomerRepository $customerRepository,
        private CreditCardRepository $creditCardRepository
    ) {
    }

    public function run(DataFile $file): void
    {
        $lock = $this->lockProcess($file);

        $filePath = $this->dataFilesService->getFilePath($file);
        $fileReader = $this->fileReaderService->makeFileReader($file->getAttribute('type'));

        $file->startImport();
        $lastIndex = $file->getAttribute('lastIndex');

        $collector = new ImportCollector();

        try {
            $collection = $fileReader->getLazyCollection($filePath);
        } catch (\Exception) {
            $file->finishImportWithError();
            $lock->release();
            return;
        }

        foreach ($collection as $row) {
            $collector->increment();

            if ($lastIndex > $collector->getIndex()) {
                continue;
            }

            try {
                $row = $this->validate($row);
            } catch (ValidationException) {
                continue;
            }

            [$customer, $card] = $this->handleRow($row);

            $collector->addCustomer($customer);
            $collector->addCard($card);

            if ($collector->getCollectedCustomers() === self::CHUNK_SIZE) {
                $this->syncPreparedDataWithStorage($collector, $file);
                $collector->flush();
            }
        }

        if (!$collector->isEmpty()) {
            $this->syncPreparedDataWithStorage($collector, $file);
            $collector->flush();
        }

        $file->finishImport();
        $lock->release();
    }

    private function handleRow(array $row): array
    {
        $card = $this->extractCard($row);
        $customer = $this->mutateCustomer($row);

        return [$customer, $card];
    }

    /**
     * @throws ValidationException
     */
    private function validate(array $row): array
    {
        $before = now()->subYears(self::MINIMAL_AGE)->toDateString();
        $after = now()->subYears(self::MAXIMAL_AGE)->toDateString();

        $validator = Validator::make($row, [
            'name'               => 'required|max:255',
            'address'            => 'required|string',
            'checked'            => 'boolean',
            'description'        => 'string',
            'interest'           => 'string|nullable',
            'date_of_birth'      => 'nullable|date|before:' . $before . '|after:' . $after,
            'email'              => 'email',
            'account'            => 'numeric',
            'credit_card'        => 'array:number,type,name,expirationDate',
            'credit_card.number' => 'numeric|digits:16',
            // If credit card must contain three consecutive numbers
            // 'credit_card.number' => 'numeric|digits:16|regex:/([0-9])\1\1/i',
        ]);

        return $validator->validate();
    }

    private function mutateCustomer(array $customer): array
    {
        $customer['date_of_birth'] = $customer['date_of_birth'] ? Carbon::make($customer['date_of_birth']) : null;

        return $customer;
    }

    private function extractCard(array &$row): array
    {
        $card = $row['credit_card'];
        unset($row['credit_card']);

        $card['expirationDate'] = CreditCard::makeDateTimeFromExpiration($card['expirationDate']);

        $row['card_id'] = $card['uuid'] = Uuid::uuid4();

        return $card;
    }

    private function syncPreparedDataWithStorage(ImportCollector $collector, DataFile $file): void
    {
        DB::transaction(function () use ($collector, $file) {
            $this->creditCardRepository->batchCreate($collector->getCards());
            $this->customerRepository->batchCreate($collector->getCustomers());

            $file->setAttribute('lastProcessedIndex', $collector->getIndex());
            $file->save();
        });
    }

    private function lockProcess(DataFile $file): Lock
    {
        $lock = Cache::lock(LockHelper::makeLockKey($file), self::DEFAULT_LOCK_TTL, self::class);
        $lock->get();

        return $lock;
    }
}