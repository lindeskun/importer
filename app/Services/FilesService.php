<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\DataFile;
use App\Repositories\DataFileRepository;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FilesService
{
    private const FILES_DIRECTORY = 'dataFiles';

    public function __construct(
        private FileReaderService $fileReaderService,
        private DataFileRepository $dataFileRepository
    ) {
    }

    public function isFileSupported(UploadedFile $uploaded): bool
    {
        return $this->fileReaderService->isFileTypeSupported($uploaded->getClientMimeType());
    }

    public function upload(UploadedFile $uploaded): DataFile
    {
        $hash = hash_file('sha256', $uploaded->getRealPath());
        $uploaded->storeAs(self::FILES_DIRECTORY, $hash);

        $file = $this->dataFileRepository->findByHash($hash);

        if (null !== $file) {
            return $file;
        }

        $type = $uploaded->getClientMimeType();
        return $this->dataFileRepository->create($hash, $type);
    }

    public function getFilePath(DataFile $file): ?string
    {
        $path = sprintf('%s/%s', self::FILES_DIRECTORY, $file->getAttribute('hash'));
        $disk = Storage::disk();
        return $disk->exists($path) ? Storage::path($path) : null;
    }
}