<?php

declare(strict_types=1);


namespace App\Services;

use App\Models\FileReaders\FileReaderInterface;
use App\Models\FileReaders\UnsupportedFileReader;

class FileReaderService
{
    /**
     * @param FileReaderInterface[] $readers
     */
    public function __construct(private array $readers)
    {
    }

    public function makeFileReader(string $mimeType): FileReaderInterface
    {
        foreach ($this->readers as $reader) {
            if ($reader->supports($mimeType)) {
                return $reader;
            }
        }

        return new UnsupportedFileReader();
    }

    public function isFileTypeSupported(string $mimeType): bool
    {
        foreach ($this->readers as $reader) {
            if ($reader->supports($mimeType)) {
                return true;
            }
        }

        return false;
    }
}