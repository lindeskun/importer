<?php

namespace App\Providers;

use App\Services\FileReaderService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FileReaderService::class, function ($app) {
            $collection = new Collection($app['config']['importer']['readers'] ?? []);

            $collection = $collection
                ->filter(function (string $className) {
                    return class_exists($className);
                })
                ->map(function (string $className) {
                    return App::make($className);
                });

            return new FileReaderService($collection->toArray());
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
