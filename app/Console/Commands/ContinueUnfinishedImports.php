<?php

namespace App\Console\Commands;

use App\Helpers\LockHelper;
use App\Jobs\ProcessDataFile;
use App\Models\DataFile;
use App\Repositories\DataFileRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class ContinueUnfinishedImports extends Command
{
    private const CHUNK_SIZE = 10;

    public function __construct(private DataFileRepository $dataFileRepository)
    {
        parent::__construct();
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:import:unfinished';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to restarting unfinished import processes';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->dataFileRepository->unfinishedImportsByChunk(self::CHUNK_SIZE, static function (array $chunk) {
            /** @var DataFile $file */
            foreach ($chunk as $file) {
                if (Cache::lock(LockHelper::makeLockKey($file))->get()) {
                    ProcessDataFile::dispatch($file);
                }
            }
        });
        return 0;
    }
}
