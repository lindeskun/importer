<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessDataFile;
use App\Services\FilesService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class UploadController extends Controller
{
    public function __construct(private FilesService $dataFilesService)
    {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $file = $request->file('dataFile');

        if (null === $file) {
            return new JsonResponse(
                ['error' => 'Empty file'],
                Response::HTTP_BAD_REQUEST,
            );
        }

        if (!$this->dataFilesService->isFileSupported($file)) {
            return new JsonResponse(
                ['error' => 'Unsupported file type'],
                Response::HTTP_BAD_REQUEST,
            );
        }

        $dataFile = $this->dataFilesService->upload($file);

        if ($dataFile->isFinished()) {
            return new JsonResponse(
                [],
                Response::HTTP_ALREADY_REPORTED
            );
        }

        ProcessDataFile::dispatch($dataFile);

        return new JsonResponse(
            [],
            Response::HTTP_ACCEPTED
        );
    }
}
