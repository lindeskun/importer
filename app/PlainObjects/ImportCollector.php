<?php

declare(strict_types=1);

namespace App\PlainObjects;

class ImportCollector
{
    private array $customersBag = [];
    private array $cardsBag = [];
    private int $index = 0;

    public function getCollectedCustomers(): int
    {
        return count($this->customersBag);
    }

    public function isEmpty(): bool
    {
        return (count($this->customersBag) + count($this->cardsBag)) === 0;
    }

    public function increment(): void
    {
        $this->index++;
    }

    public function getIndex(): int
    {
        return $this->index;
    }

    public function getCustomers(): array
    {
        return $this->customersBag;
    }

    public function getCards(): array
    {
        return $this->cardsBag;
    }

    public function addCustomer(array $row): void
    {
        $this->customersBag[] = $row;
    }

    public function addCard(array $row): void
    {
        $this->cardsBag[] = $row;
    }

    public function flush(): void
    {
        $this->customersBag = [];
        $this->cardsBag = [];
    }
}