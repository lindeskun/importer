# Files Import Application

## How to build

Install dependencies

```sh
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    laravelsail/php81-composer:latest \
    composer create-project
```

Run docker-compose with Sail
```sh
./vendor/bin/sail up
```

```sh
./vendor/bin/sail artisan migrate:fresh
```

## How to run

POST your file to:
http://localhost/api/upload

And run worker with

```sh
./vendor/bin/sail artisan queue:listen
```