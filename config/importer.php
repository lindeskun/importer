<?php

declare(strict_types=1);

use App\Models\FileReaders\JsonFileReader;

return [
    'readers' => [
        JsonFileReader::class
    ],
];